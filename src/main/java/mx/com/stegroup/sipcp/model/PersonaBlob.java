package mx.com.stegroup.sipcp.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "personas")
public class PersonaBlob implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2078169227864043086L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idPersona;
	private String nombre;
	private String apellidos;
	private String rfc;
	private String direccion;
	private Integer permisoMatar;
	// image bytes can have large lengths so we specify a value
	// which is more than the default length for picByte column
	@Column(name = "foto", length = 1000)
	private byte[] foto;
	@Column(name = "firma", length = 1000)
	private byte[] firma;
}
