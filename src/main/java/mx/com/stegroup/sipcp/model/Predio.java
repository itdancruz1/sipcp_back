package mx.com.stegroup.sipcp.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "predios")
public class Predio implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -993663674298986323L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idPredio;
	private String predio;
	private String direccion;
}
