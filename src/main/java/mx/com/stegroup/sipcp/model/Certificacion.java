package mx.com.stegroup.sipcp.model;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "certificaciones")
public class Certificacion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6571750764041846161L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idCertificacion;
	private String numCert;
	private String factura;
	private String vendedor;
	private String domicilioVendedor;
	private String comprador;
	private String domicilioComprador;
	private String numPagoAyto;
	private String folioGanaderia;
	private String transito;
	private Integer vacas;
	private Integer novillonas;
	private Integer novillos;
	private Integer toros;
	private Integer toretes;
	private Integer becerros;
	private Integer becerras;
	private Integer equinos;
	private Integer bufalos;
	private String color;
	private String motivo;
	private Date fecha;
	private String antecedentes;
	private Integer idPatente;

}
