package mx.com.stegroup.sipcp.model;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "patentes")
public class Patente implements Serializable {

	public Patente(Integer idPatente, String numPatente) {

		this.idPatente = idPatente;
		this.numPatente = numPatente;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -7895592714208312340L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idPatente;
	private String numPatente;
	private Integer idPersona;
	private Integer idPredio;
	private Date fechaInicio;
	private Date fechaFin;
	@Column(name = "fierro", length = 1000)
	private byte[] fierro;
	@Column(name = "c_bovino")
	private Integer cBovino;
	@Column(name = "c_equino")
	private Integer cEquino;
	@Column(name = "c_aves")
	private Integer cAves;
	@Column(name = "c_otros")
	private Integer cOtros;

}
