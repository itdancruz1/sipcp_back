package mx.com.stegroup.sipcp.model;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "permisos")
public class Permiso {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idPermiso;
	private String folio;
	private String municipio;
	private String gan;
	private String san;
	private String tto;
	private String fact;
	private String antecedentes;
	private String permisionario;
	private String sacrificio;
	private Date fechaSacrificio;
	private String vendedor;
	private String domicilio;
	private String procedencia;
	private String localSacrificio;
	private Date fechaEmision;

}
