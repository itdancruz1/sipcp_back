package mx.com.stegroup.sipcp.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class PayloadResponse implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -8991671392680920060L;
    private String mensaje;
    private String codigoError;
    private Object respuesta;
}
