package mx.com.stegroup.sipcp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SipcpApplication {

	public static void main(String[] args) {
		SpringApplication.run(SipcpApplication.class, args);
	}

}
