package mx.com.stegroup.sipcp.controller;

import java.io.IOException;
import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import lombok.extern.slf4j.Slf4j;
import mx.com.stegroup.sipcp.Utils;
import mx.com.stegroup.sipcp.model.Patente;
import mx.com.stegroup.sipcp.model.PayloadResponse;
import mx.com.stegroup.sipcp.model.Permiso;
import mx.com.stegroup.sipcp.model.Predio;
import mx.com.stegroup.sipcp.model.TipoGanado;
import mx.com.stegroup.sipcp.repository.PatentesRepository;
import mx.com.stegroup.sipcp.repository.PermisosRepository;
import mx.com.stegroup.sipcp.repository.PrediosRepository;
import mx.com.stegroup.sipcp.service.PatentesService;
import mx.com.stegroup.sipcp.service.TipoGanadoService;

@Slf4j
@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
@RequestMapping("sipcp/")
public class SipcpController {

	@Autowired
	PatentesService patentesService;
	@Autowired
	TipoGanadoService tipoGanadoService;
	@Autowired
	PrediosRepository prediosRepo;
	@Autowired
	PatentesRepository patentesRepo;
	@Autowired
	PermisosRepository permisosRepo;

	@GetMapping("tipo_ganado")
	@ResponseBody
	public List<TipoGanado> getTipoGanado() {
		return tipoGanadoService.getTodosTiposGanado();
	}

	@GetMapping("predios")
	@ResponseBody
	public List<Predio> getPredios(@RequestParam(required = false) String filtro) {
		if (null == filtro || "".equals(filtro)) {
			log.info("Retornar todos los predios");
			return prediosRepo.findAll();
		} else {
			log.info("Retornar predios filtrados por {}", filtro);
			return prediosRepo.findPrediosPorFiltro(filtro);
		}

	}

	@GetMapping("predios/{id}")
	@ResponseBody
	public Predio getPersona(@PathVariable int id) {
		log.info("Peticion recibida para la persona {id}", id);

		Optional<Predio> result = prediosRepo.findById(id);
		if (result.isPresent()) {
			return result.get();
		} else {
			return new Predio();
		}

	}

	@PostMapping("predios")
	@ResponseBody
	public PayloadResponse postPredio(@RequestBody Predio predio) {
		log.info("Peticion recibida para guardar {}", predio);
		PayloadResponse pr = new PayloadResponse();
		try {
			pr.setCodigoError("C0001");
			pr.setMensaje("Persona guardada");

			predio = prediosRepo.save(predio);
			pr.setRespuesta(predio);

		} catch (Exception e) {
			log.error("Error al dar de alta la persona", e);
		}
		return pr;
	}

	@PostMapping("patentes")
	@ResponseBody
	public PayloadResponse setPatente(@RequestBody Patente patente) {
		PayloadResponse pr = new PayloadResponse();
		try {
			log.info("Parametro recibido {}", patente);

			pr.setCodigoError("C0001");
			pr.setMensaje("Patente guardada");

			patente = patentesService.guardarPatente(patente);

			pr.setRespuesta(patente);

		} catch (Exception e) {
			log.error("Error al dar de alta la patente", e);
		}
		return pr;
	}

	@GetMapping("patentes")
	@ResponseBody
	public List<Patente> getPatentes(@RequestParam(required = false) String filtro) {
		if (null == filtro || "".equals(filtro)) {
			log.info("Retornar todas las patentes");
			return patentesRepo.findAll();
		} else {
			log.info("Retornar patentes filtradas por {}", filtro);
			return patentesService.getPatentePorFiltro(filtro);
		}
	}

	@GetMapping("patentes/reportes")
	@ResponseBody
	public String[][] getPatentesFiltro(@RequestParam(required = false, name = "fi") Date fi,
			@RequestParam(required = false, name = "ff") Date ff) {
				log.info("Parametro recibido {} - {}", fi, ff);
		return patentesRepo.findPatentesPorFecha(fi, ff);

	}

	@PostMapping("patentes/blob")
	public String uplaodFierro(@RequestParam(name = "fierro", required = false) MultipartFile fierro,
			@RequestParam("id") int id) throws IOException {
		try {

			Optional<Patente> imgl = patentesRepo.findById(id);
			Patente img = imgl.get();
			if (null != fierro) {
				img.setFierro(Utils.compressBytes(fierro.getBytes()));
			}

			patentesRepo.save(img);

			return "Correcto";
		} catch (

		Exception e) {
			log.error("Error al guardar la foto", e);
			return null;
		}

	}

	@GetMapping(path = { "patentes/blob/{id}" })
	public Patente getFierro(@PathVariable("id") int id) throws IOException {

		final Optional<Patente> retrievedImage = patentesRepo.findById(id);

		Patente img = retrievedImage.get();
		img.setFierro(Utils.decompressBytes(retrievedImage.get().getFierro()));
		return img;
	}

	@GetMapping("patentes/{numCert}")
	@ResponseBody
	public Patente getPatente(@PathVariable String numCert) {
		// return patentesService.getPatentePorNumero(numCert);
		return new Patente();
	}

	@PostMapping("permisos")
	@ResponseBody
	public PayloadResponse savePermiso(@RequestBody Permiso permiso) {
		PayloadResponse pr = new PayloadResponse();
		try {
			log.info("Parametro recibido {}", permiso);

			pr.setCodigoError("C0001");
			pr.setMensaje("Permiso guardado");

			permiso = permisosRepo.save(permiso);

			pr.setRespuesta(permiso);

		} catch (Exception e) {
			log.error("Error al dar de alta el permiso", e);
		}
		return pr;
	}
	
	@GetMapping("permisos/reportes")
	@ResponseBody
	public String[][] getPermisosFiltro(@RequestParam(required = false, name = "fi") Date fi,
			@RequestParam(required = false, name = "ff") Date ff) {
				log.info("Parametro recibido {} - {}", fi, ff);
		return permisosRepo.findPermisosPorFecha(fi, ff);

	}

}
