package mx.com.stegroup.sipcp.controller;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import lombok.extern.slf4j.Slf4j;
import mx.com.stegroup.sipcp.Utils;
import mx.com.stegroup.sipcp.model.PayloadResponse;
import mx.com.stegroup.sipcp.model.Persona;
import mx.com.stegroup.sipcp.model.PersonaBlob;
import mx.com.stegroup.sipcp.repository.PersonasBlobRepository;
import mx.com.stegroup.sipcp.service.PersonasService;

@Slf4j
@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
@RequestMapping("sipcp/")
public class PersonasController {
	@Autowired
	PersonasService personasService;

	@GetMapping("personas/{id}")
	@ResponseBody
	public Persona getPersona(@PathVariable int id) {
		log.info("Peticion recibida para la persona {id}", id);

		return personasService.getPersonaPorId(id);
	}

	@GetMapping("personas")
	@ResponseBody
	public List<Persona> getPersona(@RequestParam(required = false) String filtro) {
		if (null == filtro || "".equals(filtro)) {
			log.info("Retornar todas las personas");
			return personasService.getTodasPersonas();
		} else {
			log.info("Retornar personas filtradas por {}", filtro);
			return personasService.getPersonasPorFiltro(filtro);
		}

	}

	@PostMapping("personas")
	@ResponseBody
	public PayloadResponse postPersona(@RequestBody Persona persona) {
		log.info("Peticion recibida para guardar {}", persona);
		PayloadResponse pr = new PayloadResponse();
		try {
			pr.setCodigoError("C0001");
			pr.setMensaje("Persona guardada");

			persona = personasService.guardaPersona(persona);
			pr.setRespuesta(persona);

		} catch (Exception e) {
			log.error("Error al dar de alta la persona", e);
		}
		return pr;
	}

	@Autowired
	PersonasBlobRepository imageRepository;

	@PostMapping("personas/blob")
	public String uplaodImage(@RequestParam(name = "foto", required = false) MultipartFile foto,
			@RequestParam(name = "firma", required = false) MultipartFile firma, @RequestParam("id") int id)
			throws IOException {
		try {

			Optional<PersonaBlob> imgl = imageRepository.findById(id);
			PersonaBlob img = imgl.get();
			img.setIdPersona(id);
			if (null != foto) {
				img.setFoto(Utils.compressBytes(foto.getBytes()));
			}
			if (null != firma) {
				img.setFirma(Utils.compressBytes(firma.getBytes()));
			}

			imageRepository.save(img);

			return "Correcto";
		} catch (

		Exception e) {
			log.error("Error al guardar la foto", e);
			return null;
		}

	}

	@GetMapping(path = { "personas/blob/{id}" })
	public PersonaBlob getImage(@PathVariable("id") int id) throws IOException {

		final Optional<PersonaBlob> retrievedImage = imageRepository.findById(id);

		PersonaBlob img = retrievedImage.get();
		img.setFirma(Utils.decompressBytes(retrievedImage.get().getFirma()));
		img.setFoto(Utils.decompressBytes(retrievedImage.get().getFoto()));
		return img;
	}

}
