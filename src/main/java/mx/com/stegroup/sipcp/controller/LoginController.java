package mx.com.stegroup.sipcp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;
import mx.com.stegroup.sipcp.model.Usuario;
import mx.com.stegroup.sipcp.repository.UsuariosRepository;

@Slf4j
@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
@RequestMapping("sipcp/")
public class LoginController {

    @Autowired
    UsuariosRepository usuariosRepo;

    @GetMapping("usuarios")
    @ResponseBody
    public Usuario getUsuario(@RequestParam(name = "usuario") String usuario,
            @RequestParam(name = "pass") String pass) {
        log.info("Peticion recibida para login de {}", usuario);
        List<Usuario> res = usuariosRepo.getLogin(usuario, pass);
        if (null == res || res.isEmpty()) {
            return new Usuario();
        } else {
            return res.get(0);
        }

    }
}
