package mx.com.stegroup.sipcp.controller;


import java.sql.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;
import mx.com.stegroup.sipcp.model.Certificacion;
import mx.com.stegroup.sipcp.model.PayloadResponse;
import mx.com.stegroup.sipcp.repository.CertificacionesRepository;
import mx.com.stegroup.sipcp.service.CertificacionesService;

@Slf4j
@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
@RequestMapping("sipcp/")
public class CertificacionesController {

	@Autowired
	CertificacionesService certificacionesService;
	@Autowired 
	CertificacionesRepository certificacionesRepo;

	@PostMapping("certificaciones")
	@ResponseBody
	public PayloadResponse guardaCertficicacion(@RequestBody Certificacion certificacion) {
		PayloadResponse pr = new PayloadResponse();
		try {
			pr.setCodigoError("C0001");
			pr.setMensaje("Certificacion guardada");

			certificacion = certificacionesService.guardaCertificacion(certificacion);

			pr.setRespuesta(certificacion);
		} catch (Exception e) {
			log.error("Error al dar de alta la certificacion", e);
		}
		return pr;
	}

	@GetMapping("certificaciones/{numCert}")
	@ResponseBody
	public Certificacion getCertificacion(@PathVariable String numCert) {
		// return patentesService.getPatentePorNumero(numCert);
		return new Certificacion();
	}

	@GetMapping("certificaciones/reportes")
	@ResponseBody
	public String[][] getCerticacionesFiltro(@RequestParam(required = false, name = "fi") Date fi,
			@RequestParam(required = false, name = "ff") Date ff) {
				log.info("Parametro recibido {} - {}", fi, ff);
		return certificacionesRepo.findCertificacionesPorFecha(fi, ff);

	}

}
