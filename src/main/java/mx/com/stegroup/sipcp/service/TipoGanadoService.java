package mx.com.stegroup.sipcp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.stegroup.sipcp.model.TipoGanado;
import mx.com.stegroup.sipcp.repository.TipoGanadoRepository;

@Service
public class TipoGanadoService {
	@Autowired
	TipoGanadoRepository tipoGanadoRepo;

	public List<TipoGanado> getTodosTiposGanado() {
		return tipoGanadoRepo.findAll();
	}

}
