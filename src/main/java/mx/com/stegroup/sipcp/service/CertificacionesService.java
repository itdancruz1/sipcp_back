package mx.com.stegroup.sipcp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.stegroup.sipcp.model.Certificacion;
import mx.com.stegroup.sipcp.repository.CertificacionesRepository;

@Service
public class CertificacionesService {

	@Autowired
	CertificacionesRepository certificacionesRepo;

	public Certificacion guardaCertificacion(Certificacion certificacion) {
		return certificacionesRepo.save(certificacion);
	}
}
