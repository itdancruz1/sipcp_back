package mx.com.stegroup.sipcp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.stegroup.sipcp.model.Permiso;
import mx.com.stegroup.sipcp.repository.PermisosRepository;

@Service
public class PermisosService {

	@Autowired
	PermisosRepository permisosRepo;

	/// TODO:Busqueda de patentes
//	public Patente getPatentePorNumero(String numPatente) {
//		patentesRepo.fi
//	}

	public Permiso guardarPermiso(Permiso permiso) {
		return permisosRepo.save(permiso);
	}
}
