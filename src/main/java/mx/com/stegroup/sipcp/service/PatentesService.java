package mx.com.stegroup.sipcp.service;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.stegroup.sipcp.model.Patente;
import mx.com.stegroup.sipcp.repository.PatentesRepository;

@Service
public class PatentesService {

	@Autowired
	PatentesRepository patentesRepo;

	public List<Patente> getPatentePorFiltro(String filtro) {
		List<Patente> res = new ArrayList<Patente>();
		String[][] resultado = patentesRepo.findPatentesPorFiltro(filtro);

		for (int i = 0; i < resultado.length; i++) {
			Patente p = new Patente();
			p.setIdPatente(Integer.parseInt(resultado[i][0]));
			p.setNumPatente(
					resultado[i][1] + " :: " + resultado[i][2] + " " + resultado[i][3] + "[" + resultado[i][4] + "]");
			res.add(p);

		}
		return res;
	}

	public Patente guardarPatente(Patente patente) {
		return patentesRepo.save(patente);
	}
}
