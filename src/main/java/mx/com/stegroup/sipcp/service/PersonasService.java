package mx.com.stegroup.sipcp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.stegroup.sipcp.model.Persona;
import mx.com.stegroup.sipcp.repository.PersonasRepository;

@Service
public class PersonasService {
	@Autowired
	PersonasRepository personasRepo;

	public Persona guardaPersona(Persona persona) {
		return personasRepo.save(persona);
	}

	public List<Persona> getTodasPersonas() {
		return personasRepo.findAll();
	}

	public Persona getPersonaPorId(int id) {
		Optional<Persona> res = personasRepo.findById(id);
		return res.get();
	}

	public List<Persona> getPersonasPorFiltro(String filtro) {
		return personasRepo.getAllPersonasByNombre(filtro);
	}
}
