package mx.com.stegroup.sipcp.repository;


import java.sql.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import mx.com.stegroup.sipcp.model.Certificacion;

@Repository
public interface CertificacionesRepository extends JpaRepository<Certificacion, Integer> {

    @Query(value = "SELECT id_certificacion, num_cert, factura, vendedor, domicilio_vendedor, comprador, domicilio_comprador, num_pago_ayto, folio_ganaderia, transito, vacas, novillonas, novillos, toros, toretes, becerros, becerras, equinos, bufalos, color, motivo, fecha, antecedentes, id_patente from certificaciones where fecha between :fi and :ff", nativeQuery = true)
    String[][] findCertificacionesPorFecha(@Param("fi") final Date fi, @Param("ff") final Date ff);

}
