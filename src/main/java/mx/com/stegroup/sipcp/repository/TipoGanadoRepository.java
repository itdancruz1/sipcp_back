package mx.com.stegroup.sipcp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.com.stegroup.sipcp.model.TipoGanado;

public interface TipoGanadoRepository extends JpaRepository<TipoGanado, Integer> {

}
