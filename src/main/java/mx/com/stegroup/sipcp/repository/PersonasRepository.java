package mx.com.stegroup.sipcp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import mx.com.stegroup.sipcp.model.Persona;

@Repository
public interface PersonasRepository extends JpaRepository<Persona, Integer> {

	@Query("SELECT new Persona(p.idPersona, p.nombre, p.apellidos, p.rfc, p.direccion, p.permisoMatar) "
			+ " FROM Persona p "
			+ "WHERE p.nombre like :filtro% or apellidos like :filtro% ORDER BY p.apellidos")
	List<Persona> getAllPersonasByNombre(@Param("filtro") final String filtro);

}
