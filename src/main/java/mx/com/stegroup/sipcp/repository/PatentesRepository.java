package mx.com.stegroup.sipcp.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import mx.com.stegroup.sipcp.model.Patente;

@Repository
public interface PatentesRepository extends JpaRepository<Patente, Integer> {

        @Query(value = "SELECT p.id_patente, p.num_patente, ps.nombre, ps.apellidos, ps.rfc  "
                        + "FROM patentes p join personas ps on ps.id_persona=p.id_persona "
                        + "and p.num_patente like :filtro% or ps.nombre like :filtro% or ps.apellidos like :filtro% ", nativeQuery = true)
        String[][] findPatentesPorFiltro(@Param("filtro") final String filtro);

        @Query(value = "SELECT " + "p.id_patente, ps.nombre, ps.apellidos, ps.rfc, ps.direccion, case ps.permiso_matar when '1' then 'SI' else 'NO' end permiso_matar, "
                        + " pr.predio, pr.direccion predio_direccion,  p.num_patente, p.fecha_inicio, "
                        + "p.fecha_fin, p.c_bovino, p.c_equino, p.c_aves, p.c_otros " + "FROM patentes p "
                        + "join personas ps on ps.id_persona=p.id_persona "
                        + "join predios pr on pr.id_predio=p.id_predio "
                        + "where p.fecha_fin between :fi and :ff ", nativeQuery = true)
        String[][] findPatentesPorFecha(@Param("fi") final Date fi, @Param("ff") final Date ff);

}
