package mx.com.stegroup.sipcp.repository;

import java.sql.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import mx.com.stegroup.sipcp.model.Permiso;

@Repository
public interface PermisosRepository extends JpaRepository<Permiso, Integer> {

    @Query(value = "SELECT id_permiso, folio, municipio, gan, san, tto, fact, antecedentes, permisionario, sacrificio, fecha_sacrificio, vendedor, domicilio, procedencia, local_sacrificio, fecha_emision from permisos where fecha_sacrificio between :fi and :ff", nativeQuery = true)
    String[][] findPermisosPorFecha(@Param("fi") final Date fi, @Param("ff") final Date ff);

}
