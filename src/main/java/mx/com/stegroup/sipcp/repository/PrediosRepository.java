package mx.com.stegroup.sipcp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import mx.com.stegroup.sipcp.model.Predio;

public interface PrediosRepository extends JpaRepository<Predio, Integer> {

    @Query("SELECT new Predio(p.idPredio, p.predio, p.direccion) FROM Predio p "
            + "WHERE p.predio like %:filtro% or p.direccion like %:filtro% ORDER BY p.predio")
    List<Predio> findPrediosPorFiltro(@Param("filtro") final String filtro);

}
