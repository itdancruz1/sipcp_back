package mx.com.stegroup.sipcp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mx.com.stegroup.sipcp.model.PersonaBlob;

@Repository
public interface PersonasBlobRepository extends JpaRepository<PersonaBlob, Integer> {

}
