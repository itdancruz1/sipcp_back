package mx.com.stegroup.sipcp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import mx.com.stegroup.sipcp.model.Usuario;

@Repository
public interface UsuariosRepository extends JpaRepository<Usuario, Integer> {

    @Query("SELECT new Usuario(u.idUsuario, u.usuario, u.pass, u.tipo, u.nombre, u.puesto) FROM Usuario u "
            + "WHERE u.usuario= :usuario and  u.pass=md5(:pass)")
    List<Usuario> getLogin(@Param("usuario") final String usuario, @Param("pass") final String pass);

}
