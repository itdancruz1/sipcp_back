drop database sipcp;
create database sipcp;
use sipcp;

CREATE TABLE cat_tipo_ganado(  
  id_tipo_ganado int NOT NULL primary key AUTO_INCREMENT comment 'primary key AI',
  tipo_ganado varchar(20) comment 'Tipo de ganado',
  sexo varchar(50) comment 'Hembra o Macho'
) default charset utf8 comment 'Tipo de ganado';
insert into cat_tipo_ganado values (1,'Vaca','H'),
                                   (2,'Novillona','H'),
                                   (3,'Becerra','H'),
                                   (4,'Novillo','M'),
                                   (5,'Toro','M'),
                                   (6,'Torete','M'),
                                   (7,'Becerro','M'),
                                   (8,'Equino','M'),
                                   (9,'Bufalo','M'),
                                   (10,'Bufalo Hembra','H');
COMMIT ;

CREATE TABLE personas(  
  id_persona int NOT NULL primary key AUTO_INCREMENT comment 'primary key AI',
  nombre varchar(20) comment 'Nombre de la persona',
  apellidos varchar(50) comment 'Apellidos de la persona',
  rfc varchar(20) comment 'RFC de la persona',
  direccion varchar(200) comment 'Direccion de la persona',
  permiso_matar TINYINT comment '0 para especificar sin permiso, 1 para especificar que si tiene permiso',
  foto mediumblob,
  firma mediumblob
) default charset utf8 comment 'Informacion de las personas que registran su ganado';

CREATE TABLE predios(  
  id_predio int NOT NULL primary key AUTO_INCREMENT comment 'primary key AI',
  predio varchar(300) comment 'Nombre del predio',
  direccion varchar(500) comment 'Direccion del predio'
) default charset utf8 comment 'Predios registrados por las personas';

CREATE TABLE patentes(  
  id_patente int NOT NULL primary key AUTO_INCREMENT comment 'primary key AI',
  num_patente varchar(20) comment 'Numero asignado a la patente',
  id_persona INT,
  id_predio int,
  propiedad_predio VARCHAR (20) COMMENT 'Renta o Propio',
  fecha_inicio DATETIME COMMENT 'Inicio de la patente',
  fecha_fin DATETIME COMMENT 'Termino de la patente',
  fierro mediumblob 'Foto del fierro que se patenta',
  c_bovino int,
  c_equino int,
  c_aves int,
  c_otros int,
  created_time DATETIME COMMENT 'created time'
) default charset utf8 comment 'Registro patentes';
ALTER TABLE patentes ADD foreign key (id_persona) references personas (id_persona);
ALTER TABLE patentes ADD foreign key (id_predio) references predios (id_predio);

CREATE TABLE permisos(  
  id_permiso int NOT NULL primary key AUTO_INCREMENT comment 'primary key AI',
  folio varchar(300) comment 'Tipo de ganado (Bovino, equino...)',
  municipio varchar(200) comment 'Cantidad de ganado registrado',
  gan varchar(200),
  san varchar(200),
  tto varchar(200),
  fact VARCHAR (200),
  antecedentes VARCHAR (200),
  permisionario VARCHAR(200) NULL DEFAULT NULL,
  sacrificio VARCHAR (500),
  fecha_sacrificio DATETIME ,
  vendedor VARCHAR (300),
  domicilio VARCHAR (300),
  procedencia VARCHAR (300),
  local_sacrificio VARCHAR (200),
  fecha_emision DATETIME
) default charset utf8 comment 'Permisos de sacrificio emitidos por el municipio';

CREATE TABLE certificaciones(  
  id_certificacion int NOT NULL primary key AUTO_INCREMENT comment 'primary key AI',
  num_cert varchar(30) comment 'Numero de certificacion en el libro',
  factura varchar(50) comment 'Numero de factura',
  vendedor VARCHAR (500),
  domicilio_vendedor VARCHAR (300),
  comprador VARCHAR (500),
  domicilio_comprador VARCHAR (300),
  num_pago_ayto VARCHAR (100),
  folio_ganaderia VARCHAR (100),
  transito VARCHAR (100) COMMENT 'Numero de guia',
  vacas int,
  novillonas int,
  novillos int,
  toros int,
  toretes int,
  becerros int,
  becerras int,
  equinos int,
  bufalos int,
  color VARCHAR (200),
  motivo VARCHAR (100),
  fecha DATE ,
  antecedentes VARCHAR (300),
  id_patente int

) default charset utf8 comment 'Certificaciones';


CREATE TABLE usuarios (
  `id_usuario` INT NOT NULL AUTO_INCREMENT,
  `usuario` VARCHAR(45) NULL,
  `tipo` VARCHAR(45) NULL,
  `nombre` VARCHAR(200) NULL,
  `pass` VARCHAR(80) NULL,
  `puesto` VARCHAR(100) NULL,
  PRIMARY KEY (`id_usuario`));
  
  INSERT INTO `usuarios` ( `usuario`, `tipo`, `nombre`, `pass`, `puesto`) VALUES ( 'admin', 'Administrador', 'Administrador', md5('admin'), 'Administrador');
commit;

